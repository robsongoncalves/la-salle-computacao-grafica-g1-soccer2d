#include "glut.h"
#include <stdlib.h>
#include <stdlib.h>
#include <stdio.h>
#include<time.h>

// funcao que implementa o itoa no gcc
char* itoa(int value, char* result, int base) {
  // verifica se a base e' valida
  if (base < 2 || base > 36) { *result = '\0'; return result; }
  char* ptr = result, *ptr1 = result, tmp_char;
  int tmp_value;

  do {
    tmp_value = value;
    value /= base;
    *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
  } while ( value );

  // Aplica sinal negativo
  if (tmp_value < 0) *ptr++ = '-';
  *ptr-- = '\0';
  while(ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr--= *ptr1;
    *ptr1++ = tmp_char;
  }
  return result;
}

void delay(unsigned int mseconds)
{
    clock_t goal = mseconds + clock();
    while (goal > clock());
}

double PosicaoIJogador[2][2] = { {0.4,0.5},{0.6, 0.5} };
double PosicaoIBola[2] = { 0.5, 0.5 };
    // qtd de jogadores
double Jogador[2][2] = { {0.4,0.5},{0.6, 0.5} };
double Bola[2] = { 0.5, 0.5 };
int Placar[2] = { 0, 0 };
    // x e y
int forcaChute = 11;
    // indice de posicoes - x e y
// indice 0 = bola
GLUquadricObj *quadObj;

void Draw(){
    DesenhaCampo();

    DesenhaMeioCampo();

    DesenhaAreaEsquerda();

    DesenhaAreaDireita();

    DesenhaPlacar();

    // DesenhaTempo();

    DesenhaJogadorTime1(0); //jogador1

    DesenhaJogadorTime2(1); //jogador2

    DesenhaBola();

    //gluPartialDisk(quadObj,0.0,100.0,100,100, 270.0,180.0);
    glPopMatrix();
    glFlush();
}

void Gol(int time) {
    int i=0, z=0;

    Placar[time]++;

    //zera posicoes
    for(i=0; i<2; i++) {
        for(z=0; z<2; z++) {
            Jogador[i][z] = PosicaoIJogador[i][z];
        }
    }
    Bola[0] = PosicaoIBola[0];
    Bola[1] = PosicaoIBola[1];
}

void DesenhaPlacar()
{
    int i=0;
    char placar[70];
    for(i=0; i<70; i++)
        placar[i] = '\0';
    itoa(Placar[0],placar,10);
    itoa(Placar[1],placar,10);
    sprintf(placar, "Time1 %d x %d Time2\0", Placar[0], Placar[1]);
  	glPushMatrix();
        // Posicao no universo onde o texto sera colocado
        glRasterPos2f(0.11, 0.91);
        // Exibe caracter a caracter
        for(i=0; i<70; i++)
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,placar[i]);
	glPopMatrix();
}


// void CalculaTempo(){
//     clock_t Ticks[2];
//     Ticks[0] = clock();
//     //O código a ter seu tempo de execução medido ficaria neste ponto.
//     Ticks[1] = clock();
//     double Tempo = (Ticks[1] - Ticks[0]) * 1000.0 / CLOCKS_PER_SEC;
//     printf("Tempo gasto: %g ms.", Tempo);
//     getchar();
//     // return 0;
//     return Tempo;
// }

// void DesenhaTempo(){
//     int i=0;
//     char placar[70]; 
//     char tempo[70];
//     for(i=0; i<70; i++)
//         placar[i] = '\0';
//     itoa(Placar[0],placar,10);
//     itoa(Placar[1],placar,10);

//     tempo = CalculaTempo();
//     // sprintf(placar, "00:00", Placar[0], Placar[1]);
//     sprintf(tempo);
//   	glPushMatrix();
//         // Posicao no universo onde o texto sera colocado
//         glRasterPos2f(0.35, 0.91);
//         // Exibe caracter a caracter
//         for(i=0; i<70; i++)
//             glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,placar[i]);
// 	glPopMatrix();
// }

void DesenhaCampo(){
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(0.0, 0.5, 0.0);
    glBegin(GL_QUAD_STRIP);
    glVertex3f(0.1, 0.1, 0.0);
    glVertex3f(0.1, 0.9, 0.0);
    glVertex3f(0.9, 0.1, 0.0); // Vertice do campo (verde)
    glVertex3f(0.9, 0.9, 0.0);
    glEnd();
}

void DesenhaMeioCampo(){
    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.5, 0.1, 0.0); // Linha do meio campo (divisor do campo)
    glVertex3f(0.5, 0.9, 0.0);
    glVertex2i(30,30);  glVertex2i(570,30);
    glVertex2i(570,370); glVertex2i(30,370);
    glEnd();
}



void DesenhaAreaEsquerda(){
    // Lado esquerdo do campo
    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.25, 0.25, 0.0); // Frente da goleira esquerda
    glVertex3f(0.25, 0.75, 0.0);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.1, 0.75, 0.0); // Lado Esquerdo da Goleira esquerda
    glVertex3f(0.25, 0.75, 0.0);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.1, 0.25, 0.0); // Lado Direito da goleira esquerda
    glVertex3f(0.25, 0.25, 0.0);
    glEnd();

    glBegin(GL_QUAD_STRIP);
    glColor3f(1, 1, 1);
    glVertex3f(0.1, 0.35, 0.0);
    glVertex3f(0.1, 0.65, 0.0); // Area interna da goleira esquerda (retangulo branco)
    glVertex3f(0.17, 0.35, 0.0);
    glVertex3f(0.17, 0.65, 0.0);
    glEnd();
}

void DesenhaAreaDireita(){
    // Lado direito do campo
    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.75, 0.25, 0.0); // Frente da goleira direita
    glVertex3f(0.75, 0.75, 0.0);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.75, 0.25, 0.0); // Lado esquerdo da goleira direita
    glVertex3f(0.9, 0.25, 0.0);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 1, 1);
    glVertex3f(0.75, 0.75, 0.0); // Lado direito da goleira direita
    glVertex3f(0.9, 0.75, 0.0);
    glEnd();

    glBegin(GL_QUAD_STRIP);
    glColor3f(1, 1, 1);
    glVertex3f(0.9, 0.35, 0.0);
    glVertex3f(0.9, 0.65, 0.0);
    glVertex3f(0.83, 0.35, 0.0); // Area interna da goleira direita (retangulo branco)
    glVertex3f(0.83, 0.65, 0.0);
    glEnd();
}


void DesenhaJogadorTime1(int indice) {
    // glColor3f(0.5, 0.5,1); 
    glColor3f(1,0,0); // azul
    glPushMatrix();
    glTranslated(Jogador[indice][0],Jogador[indice][1], 0.0);
    glScalef(0.5,0.7,1);
    gluDisk(quadObj, 0.0, 0.05, 100,100);
    //gluPartialDisk(quadObj,0.0,100.0,100,100, 270.0,180.0);
    glPopMatrix();
    glFlush();
}

void DesenhaJogadorTime2(int indice) {
    // glColor3f(1, 0.5,1); // branco
    glColor3f(0.5, 0.5,1); // vermelho
    glPushMatrix();
    // sprintf(placar, "Time1 %d x %d Time2\0", Placar[0], Placar[1]);
    glTranslated(Jogador[indice][0],Jogador[indice][1], 0.0);
    glScalef(0.5,0.7,1);
    gluDisk(quadObj, 0.0, 0.05, 100,100);
    //gluPartialDisk(quadObj,0.0,100.0,100,100, 270.0,180.0);
    glPopMatrix();
    glFlush();
}

void DesenhaBola() {
    glColor3f(1, 0.5,1); // branca
    // glColor3f(0,0,0); // preta
    glPushMatrix();
    glTranslated(Bola[0],Bola[1], 0.0);
    glScalef(0.3,0.5,1);
    gluDisk(quadObj, 0.0, 0.05, 100,100);
}

void moveBolaCima(int forca) {
    char direcao = 'C';
    if (Bola[1] < 0.85) {
        Bola[1] = Bola[1]+0.01;
    } else {
        direcao = 'B';
    }
    Draw();
    delay(100);
    forca--;
    if(forca > 0 && direcao == 'C') {
        moveBolaCima(forca);
    } else if (forca > 0 && direcao == 'B') {
        moveBolaBaixo(forca);
    }
}

void moveBolaBaixo(int forca) {
    char direcao = 'B';
    if (Bola[1] > 0.15) {
        Bola[1] = Bola[1]-0.01;
    } else {
        direcao = 'C';
    }
    Draw();
    delay(100);
    forca--;
    if(forca > 0 && direcao == 'B') {
        moveBolaBaixo(forca);
    } else if (forca > 0 && direcao == 'C') {
        moveBolaCima(forca);
    }
}

void moveBolaDireita(int forca) {
    char direcao = 'D';
    if (Bola[0] < 0.84) {
        Bola[0] = Bola[0]+0.01;
    } else if (Bola[0] >= 0.84 && Bola[1] <= 0.62 && Bola[1] >= 0.37) {
        forca=0;
        Gol(0);
    } else {
        direcao = 'E';
    }
    Draw();
    delay(100);
    forca--;
    if(forca > 0 && direcao == 'D') {
        moveBolaDireita(forca);
    } else if (forca > 0 && direcao == 'E') {
        moveBolaEsquerda(forca);
    }
}


void moveBolaEsquerda(int forca) {
    char direcao = 'E';
    if (Bola[0] > 0.17) {
        Bola[0] = Bola[0]-0.01;
    // } else if (Bola[0] <= 0.17) {
    } else if (Bola[0] <= 0.17 && Bola[1] <= 0.62 && Bola[1] >= 0.37){
        forca=0;
        Gol(1);
    } else {
        direcao = 'D';
    }
    Draw();
    delay(100);
    forca--;
    if(forca > 0 && direcao == 'D') {
        moveBolaDireita(forca);
    } else if (forca > 0 && direcao == 'E') {
        moveBolaEsquerda(forca);
    }
}

void moveCima(int indice) {
    if (Jogador[indice][1] < 0.85) {
        Jogador[indice][1] = Jogador[indice][1]+0.01;
    }
}

void moveDireita(int indice) {
    // if (Jogador[indice][0] < 0.86) {
    if (Jogador[indice][0] < 0.87) {        
        Jogador[indice][0] = Jogador[indice][0]+0.01;
    }
}

void moveEsquerda(int indice) {
    // if (Jogador[indice][0] > 0.14) {
    if (Jogador[indice][0] > 0.13) {
        Jogador[indice][0] = Jogador[indice][0]-0.01;
    }
}

void moveBaixo(int indice) {
    if (Jogador[indice][1] > 0.15) {
        Jogador[indice][1] = Jogador[indice][1]-0.01;
    }
}

void TeclasNormais(unsigned char tecla,int x, int y)
{
    // jogador 1
     if (tecla == 'q')
        exit(0);
     if (tecla == 'w') {
        moveCima(0);
     }
     if (tecla == 'a') {
        moveEsquerda(0);
     }
     if (tecla == 's') {
        moveBaixo(0);
     }
     if (tecla == 'd') {
        moveDireita(0);
     }

     // jogador 2
     if (tecla == 'i') {
        moveCima(1);
     }
     if (tecla == 'j') {
        moveEsquerda(1);
     }
     if (tecla == 'k') {
        moveBaixo(1);
     }
     if (tecla == 'l') {
        moveDireita(1);
     }

     // movimentos de bola
     if (tecla == 'd' && (Jogador[0][0]+0.03) > Bola[0] && Jogador[0][0]+0.02 <= Bola[0] && Jogador[0][1] == Bola[1]) {
        moveBolaDireita(forcaChute);
     }
     if (tecla == 'a' && (Jogador[0][0]-0.03) < Bola[0] && Jogador[0][0]-0.02 >= Bola[0] && Jogador[0][1] == Bola[1]) {
        moveBolaEsquerda(forcaChute);
     }
     if (tecla == 'w' && (Jogador[0][1]+0.06) >= Bola[1] && (Jogador[0][1]+0.04) <= Bola[1] && (float)Jogador[0][0] == (float)Bola[0]) {
        moveBolaCima(forcaChute);
     }
     if (tecla == 's' && (Jogador[0][1]-0.06) <= Bola[1] && (Jogador[0][1]-0.04) >= Bola[1] && (float)Jogador[0][0] == (float)Bola[0]) {
        moveBolaBaixo(forcaChute);
     }
     if (tecla == 'l' && (Jogador[1][0]+0.04) >= Bola[0] && Jogador[1][0]+0.02 <= Bola[0] && Jogador[1][1] == Bola[1]) {
        moveBolaDireita(forcaChute);
     }
     if (tecla == 'j' && (Jogador[1][0]-0.03) <= Bola[0] && Jogador[1][0]-0.01 >= Bola[0] && Jogador[1][1] == Bola[1]) {
        moveBolaEsquerda(forcaChute);
     }
     if (tecla == 'i' && (Jogador[1][1]+0.06) >= Bola[1] && (Jogador[1][1]+0.04) <= Bola[1] && (float)Jogador[1][0] == (float)Bola[0]) {
        moveBolaCima(forcaChute);
     }
     if (tecla == 'k' && (Jogador[1][1]-0.08) <= Bola[1] && (Jogador[1][1]-0.06) <= Bola[1] && (float)Jogador[1][0] == (float)Bola[0]) {
        moveBolaBaixo(forcaChute);
     }

     glutPostRedisplay();
}

void TeclasEspeciais(int tecla, int x, int y)
 {
  if(tecla == GLUT_KEY_RIGHT){
    quadObj;
  }

  glutPostRedisplay();
}

void Initialize()
{
    glClearColor(0.0, 0.0, 0.0, 0.20);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
}

int main(int iArgc, char** cppArgv)
{
    glutInit(&iArgc, cppArgv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(900, 450);
    glutInitWindowPosition(200, 200);
    glutCreateWindow("Football Ground OpenGL");
    quadObj = gluNewQuadric();
    glutKeyboardFunc(TeclasNormais);
    glutSpecialFunc(TeclasEspeciais);
    Initialize();
    glutDisplayFunc(Draw);
    glutMainLoop();
    return 0;
}
