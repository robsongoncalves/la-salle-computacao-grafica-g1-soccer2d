_________________________________________________________________________________________________________________________
####################################### F U N Ç Õ E S   P R I N C I P A I S #######################################
[ x ] A aplicação deverá permitir que o usuário interaja através do teclado ou mouse.
[ x ] A idéia do ambiente 2D deve ser inspirada no jogo de futebol de botão. 
[ x ] O aluno deve modelar 5 objetos:
    [ x ] bola,
    [ x ] botões, 
    [ x ] campo, 
    [ x ] trave, 
    [ x ] linhas do campo).
[ x ] Os movimentos devem ser sincronizados de maneira a imitar uma movimentação (“chute”) do jogo real controlados
pelos dispositivos de entrada.
[ x ] Transformações de rotação, escala e translação controladas pelo usuário;
[ x ] Possibilidade de interagir com “chute ao botão”
[   ] Relatório de como foi desenvolvido o modelo (modelagem, SRO de cada objeto, SRU);
[ x ] Implementar usando a biblioteca OpenGL + glut.
[ - ] O programa fonte deverá ter comentários de lógica de programação e com identificação no cabeçalho. (falar do programa e autores)
[ - ] Além disso, o aluno deverá entregar o relatório explicando a modelagem e descrevendo os controles para manipular o sw.
[ - ] Esse fonte também deverá ser entregue no moodle. O título deverá ser: T1-CG – nomesdosalunos .
[ - ] O nome do arquivo também deve ser padronizado como: NomesAlunos-cg-t1.c
[ - ] Apresentação:
    [ - ] técnicas utilizadas, 
    [ - ] relatando problemas e 
    [ - ] soluções encontradas e a 
    [ - ] aplicação desenvolvida.

_________________________________________________________________________________________________________________________
####################################### B U G S #######################################
[ x ] Do lado esquerdo basta chegar na area que se faz gol (Qualquer jogador)
[ x ] Jogador nao vai até o final da area verde do campo
[   ] Ampliar o raio de alcance da bola, para ser atingida em diagonal
[ - ] Ajustar quando a bola sai para a lateral (posicionar o jogador adversario na lateral, assim como na linha de fundo do time branco)
[ - ] Criar regra de escanteio/tiro de meta (posicionar adversario)

_________________________________________________________________________________________________________________________
####################################### M E L H O R I A S #######################################
[ x ] Placar
[   ] Adicionar Linhas do campo (meio campo, escanteio, pequena area, goleira, laterais)
[   ] Tempo
[   ] Adicionar o goleiro
[   ] Mais jogadores
[   ] Trocar desenho dos jogadores
[   ] Seleção de jogadores
[   ] Numero dos jogadores
[   ] Seleção de time
[   ] Musica
[   ] Introdução
[   ] Dividir primeiro e segundo tempo, contar e inverter lados